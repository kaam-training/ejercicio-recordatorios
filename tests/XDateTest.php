<?php

declare(strict_types=1);

namespace Kaam;

use PHPUnit\Framework\TestCase;

class XDateTest extends TestCase
{
    /**
     * @test
     */
    public function getters()
    {
        $xDate = new XDate('1789/01/24');
        self::assertEquals(1, $xDate->getMonth());
        self::assertEquals(24, $xDate->getDay());
    }

    /**
     * @test
     */
    public function isSameDate()
    {
        $xDate          = new XDate('1789/01/24');
        $sameDay        = new XDate('2001/01/24');
        $notSameDay     = new XDate('1789/01/25');
        $notSameMonth   = new XDate('1789/02/25');

        self::assertTrue($xDate->isSameDay($sameDay),          'same');
        self::assertFalse($xDate->isSameDay($notSameDay),      'not same day');
        self::assertFalse($xDate->isSameDay($notSameMonth),    'not same month');
    }

    /**
     * @test
     */
    public function equality()
    {
        $base       = new XDate("2000/01/02");
        $same       = new XDate("2000/01/02");
        $different  = new XDate("2000/01/04");

        self::assertFalse($base->equals(null));
        self::assertFalse($base->equals(''));
        self::assertTrue($base->equals($base));
        self::assertTrue($base->equals($same));
        self::assertFalse($base->equals($different));
    }

}
