<?php
declare(strict_types=1);

namespace Kaam;

use PHPUnit\Framework\TestCase;

class EmployeeTest extends TestCase
{
    /**
     * @test
     */
    public function birthday()
    {
        $employee = new Employee('foo', 'bar', '1990/01/31', 'a@b.c');
        self::assertFalse($employee->isBirthday(new XDate('2008/01/30')), 'not his birthday');
        self::assertTrue($employee->isBirthday(new XDate('2008/01/31')), 'his birthday');
    }

    /**
     * @test
     */
    public function equality()
    {
        $base       = new Employee('First', 'Last', '1999/09/01', 'first@last.com');
        $same       = new Employee('First', 'Last', '1999/09/01', 'first@last.com');
        $different  = new Employee('First', 'Last', '1999/09/01', 'boom@boom.com');

        self::assertFalse($base->equals(null));
        self::assertFalse($base->equals(''));
        self::assertTrue($base->equals($same));
        self::assertFalse($base->equals($different));
    }
}
