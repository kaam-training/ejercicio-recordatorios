<?php

declare(strict_types=1);

namespace Tests\Kaam;

use Kaam\ReminderService;
use Kaam\XDate;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use Symfony\Component\Process\Process;

class AcceptanceTest extends TestCase
{
    public const SMTP_HOST = 'mailhog';
    public const SMTP_PORT = 1025;

    /**
     * @var ReminderService
     */
    private $service;

    /** @before */
    protected function init(): void
    {
        $this->service = new ReminderService();
    }

    /** @after */
    protected function stopMailhog(): void
    {
        (new Client())->delete('http://mailhog:8025/api/v1/messages');
    }

    /**
     * @test
     */
    public function envioRecordatorioCuandoCumple(): void
    {
        $this->service->sendReminder(
            __DIR__ . '/resources/employee_data.txt',
            new XDate('1979/01/01'),
            static::SMTP_HOST,
            static::SMTP_PORT
        );

        $messages = $this->messagesSent();
        self::assertCount(1, $messages);

        $message = $messages[0];
        self::assertEquals('Hola Nicolas, te deseamos un feliz aniversario.', $message['Content']['Body']);
        self::assertEquals('Feliz aniversario!', $message['Content']['Headers']['Subject'][0]);
        self::assertCount(1, $message['Content']['Headers']['To']);
        self::assertEquals('nicolas.riesco@foobar.com', $message['Content']['Headers']['To'][0]);
    }

    /**
     * @test
     */
    public function noEnviaRecordatorioCuandoNoCumple(): void
    {
        $this->service->sendReminder(
            __DIR__ . '/resources/employee_data.txt',
            new XDate('1979/10/10'),
            static::SMTP_HOST,
            static::SMTP_PORT
        );

        self::assertCount(0, $this->messagesSent(),'Se envia email aunque no sea su aniversario.');
    }

    private function messagesSent(): array
    {
        return json_decode(file_get_contents('http://mailhog:8025/api/v1/messages'), true);
    }
}
