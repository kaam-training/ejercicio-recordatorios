# Ejercicio de recordatorio de caducidad en PHP

El objetivo de éste ejercicio es refactorizar el codigo según los principios SOLID de diseño en PHP OOP 
y las bases de TDD.

Éste ejercicio repasaremos las bases de la arquitectura hexagonal como metodología para estructurar una aplicación.


### Requerimientos

Los requerimientos para éste ejercicio son:

* Conocimientos Docker y Docker Compose.
* Conocimientos en la suite de testeo de php PHPUnit.
* Conocimientos de los principios SOLID y su aplicación.

### Problema

El codigo fuente realiza lo siguiente:

* Carga un set de empleados desde un archivo plano. (TXT, CSV).
* Envia un recordatorio por email a aquellos empleados en los que la deuda caduque dentro de una semana.

El archivo de texto plano va a contener la siguiente información.

```
firstName, lastName, email, signatureDate
Antonio, Gimenez, antonio@foobar.com, 2020/01/01
...

```

Los emails tendrán el siguiente formato:

```
Asunto: Aviso de caducidad de contrato

Hola {{nombre}}, esto es una comunicación para avisarle de que su contrato vence la próxima semana.

```

### Ejecución

Para poder ejecutar esta prueba primero debes de clonar el proyecto.

```
git clone git@gitlab.com:kaam-training/ejercicio-recordatorios.git
cd ejercicio-recordatorios
```

Para comprobar que todos los test funcionan ejecuta PHPUnit

```
docker-compose up -d mailhog
docker-compose run --rm composer2 composer install
docker-compose run --rm php74 php bin/phpunit 
```

Ahora ya puedes empezar a hackear!!

### Objetivo

Refactorizar todo el codigo fuente teniendo en cuenta que los datos podrían provenir de distintas fuentes, no tiene porque ser un archivo CSV.
Refactorizar el aviso al empleado ya que se podrian optar por otros metodos de aviso como por ejemplo enviar a traves de otro servicio api de envio de emails u otro servicio que envie notificaciones push.

Se tiene total libertad para crear cuantos ficheros sean necesarios para una buena refactorización. Lo que proponemos aquí es aplicar los principios SOLID para estructurar el codigo y que el resultado esté desacoplado de la infraestructura.

